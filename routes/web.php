<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dailybuglanding');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/allbugs', 'BugController@index');

Route::get('/indivbug/{id}', 'BugController@showIndivBug');

Route::get('/addbug', 'BugController@create');

Route::post('/addbug', 'BugController@store');

Route::get('/mybugs', 'BugController@indivBugs');

Route::delete('/deletebug/{id}', 'BugController@destroy');

Route::get('/editbug/{id}', 'BugController@edit');

Route::patch('/editbug/{id}', 'BugController@update');

Route::patch('/accept/{id}', 'BugController@accept');

Route::get('/solve/{id}', 'BugController@showSolve');

Route::post('/solve', 'BugController@saveSolution');

// to delete solution
Route::delete('/deletesolution/{id}', 'SolutionController@destroy');


