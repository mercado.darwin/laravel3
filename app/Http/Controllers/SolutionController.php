<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Solution;

class SolutionController extends Controller
{
    
    public function destroy($id) {
    	$solutionTodelte = Solution::find($id);
    	$solutionTodelte->delete();

    	return redirect()->back();
    }
}
